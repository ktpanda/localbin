[Version 0.0.3 (2023-03-08)](https://pypi.org/project/localbin/0.0.3/)
============================

* Show summary of changed files by default, with option to show full diff ([2065647](https://gitlab.com/ktpanda/localbin/-/commit/2065647f396676c31f5a61cbc14eae2c2951514e))


[Version 0.0.2 (2022-10-04)](https://pypi.org/project/localbin/0.0.2/)
============================

* Add --write-config and --only-config options ([2df6e5d](https://gitlab.com/ktpanda/localbin/-/commit/2df6e5dbcfe25b39a78f8f2986ceb509ad1ec2ca))


[Version 0.0.1 (2022-10-04)](https://pypi.org/project/localbin/0.0.1/)
============================

* Initial commit ([9135297](https://gitlab.com/ktpanda/localbin/-/commit/9135297bcc070c121b752ea266c2690c6f7b338f))
